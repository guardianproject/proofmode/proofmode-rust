mod check;

use clap::{Parser, Subcommand};
use check::check::ProofCheck;
use check::utils::MessageType;
use check::{check_cids, check_files, check_urls};

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Check {
        #[arg(short, long)]
        file: Option<Vec<String>>,

        #[arg(short, long)]
        dir: Option<Vec<String>>,

        #[arg(short, long)]
        url: Option<Vec<String>>,

        #[arg(short, long)]
        cid: Option<Vec<String>>,

        #[arg(short, long)]
        output_file: Option<String>,
    },
    Generate {
        #[arg(short, long)]
        file: Option<Vec<String>>,

        #[arg(short, long)]
        dir: Option<Vec<String>>,
    },
    Sign {
        #[arg(short, long)]
        cert: Option<String>,
    },
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Some(Commands::Check {
            file,
            dir,
            url,
            cid,
            output_file,
        }) => {
            let callback = |message_type: MessageType, message: String| {
                println!("{:?}: {}", message_type, message);
            };
            let mut result: Option<ProofCheck> = None;

            if let Some(url) = url {
                result = Some(check_urls(url, callback).unwrap());
            }

            if let Some(cid) = cid {
                result = Some(check_cids(cid, callback).unwrap());
            }

            if let Some(files) = file {
                result = Some(check_files(files, callback).unwrap());
            }

            if let Some(directories) = dir {
                let mut files = Vec::new();
                for directory in directories {
                    let paths = std::fs::read_dir(directory).unwrap();
                    for path in paths {
                        let path = path.unwrap().path();
                        let path = path.to_str().unwrap().to_string();
                        files.push(path);
                    }
                }
                result = Some(check_files(&files, callback).unwrap());
            }

            if let Some(result) = result {
                if let Some(output_file) = output_file {
                    match std::fs::write(
                        &output_file,
                        serde_json::to_string_pretty(&result).unwrap(),
                    ) {
                        Ok(_) => println!("Output written to {}", output_file),
                        Err(e) => eprintln!("Failed to write output to {}: {}", output_file, e),
                    }
                }
            }
        }
        Some(Commands::Generate { file, dir }) => {
            println!("Not implemented. Files: {:?}", file);
            println!("Not implemented: Directories: {:?}", dir);
        }
        Some(Commands::Sign { cert }) => {
            println!("Not implemented: Cert: {:?}", cert);
        }

        None => {}
    }
}
