use crate::check::consistency::{check_consistency, Consistency};
use crate::check::integrity::{get_integrity_summary, IntegritySummary};
use crate::check::preprocess::{
    prepare_files, PGPKey, ProofCheckError, ProofCheckFile, RawFile,
};
use crate::check::synchrony::{check_synchrony, Synchrony};
use crate::check::utils::{MessageType, ProgressReporter};
use chrono::Local;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
struct Metadata {
    id: String,
    file_count: u8,
    start_time: String,
    end_time: String,
    platform: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ProofCheck {
    metadata: Metadata,
    keys: Vec<PGPKey>,
    files: Vec<ProofCheckFile>,
    errors: Vec<ProofCheckError>,
    integrity: IntegritySummary,
    consistency: Consistency,
    synchrony: Synchrony,
}

pub fn check_common<T: RawFile>(files: Vec<T>, progress: &ProgressReporter) -> ProofCheck {
    progress.report(MessageType::Status, "Starting ProofCheck".to_string());
    let start_time = Local::now().to_string();

    let errors = files
        .iter()
        .filter_map(|file| {
            if file.error().is_some() {
                Some(ProofCheckError {
                    name: file.name(),
                    error: file.error().unwrap(),
                })
            } else {
                None
            }
        })
        .collect();

    progress.report(MessageType::Status, "Extracting files".to_string());
    let (proof_files, keys) = prepare_files(files);

    progress.report(MessageType::Status, "Checking integrity".to_string());
    let integrity = get_integrity_summary(&proof_files);

    progress.report(MessageType::Status, "Checking consistency".to_string());
    let consistency = check_consistency(&proof_files);

    progress.report(MessageType::Status, "Checking synchrony".to_string());
    let synchrony = check_synchrony(&proof_files);

    progress.report(MessageType::Status, "Preparing results".to_string());
    let metadata = Metadata {
        id: Uuid::new_v4().to_string(),
        file_count: proof_files.len() as u8,
        start_time,
        end_time: Local::now().to_string(),
        platform: "Unknown".to_string(),
    };

    ProofCheck {
        metadata,
        keys,
        integrity,
        consistency,
        synchrony,
        files: proof_files,
        errors,
    }
}
