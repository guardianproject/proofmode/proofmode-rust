use thiserror::Error;
#[cfg(not(feature = "wasm"))]
use reqwest::Error as ReqwestError;

#[derive(Error, Debug)]
pub enum ProofModeError {
    #[error("Failed with message: {message}")]
    Failure { message: String },

    // #[error("unexpected id: {id}")]
    // Unexpected { id: i32 },
}

impl From<std::io::Error> for ProofModeError {
    fn from(error: std::io::Error) -> Self {
        ProofModeError::Failure {
            message: error.to_string(),
        }
    }
}

impl From<serde_json::Error> for ProofModeError {
    fn from(error: serde_json::Error) -> Self {
        ProofModeError::Failure {
            message: error.to_string(),
        }
    }
}

#[cfg(not(feature = "wasm"))]
impl From<ReqwestError> for ProofModeError {
    fn from(error: ReqwestError) -> Self {
        ProofModeError::Failure {
            message: error.to_string(),
        }
    }
}
