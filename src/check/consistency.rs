use crate::check::preprocess::ProofCheckFile;

use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::Value;


#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Consistency {
    outliers: Value,
    verified: bool,
}

pub fn check_consistency(_proof_files: &Vec<ProofCheckFile>) -> Consistency {
    /*
    let mut all_json = vec![];
    for file in proof_files {
        if let Some(json) = &file.json {
            all_json.push(json.clone());
        }
    }

    let json_string = serde_json::to_string(&all_json).unwrap();
    let json_str = json_string.as_str();
    let cursor = Cursor::new(json_str.as_bytes());
    let df = JsonReader::new(cursor).finish().unwrap();
    let new_df = df
        .unnest(["file", "device", "location", "network", "extended"])
        .unwrap();

    let lat_column = new_df.column("latitude").unwrap();

    let lat_mean = lat_column.mean().unwrap();
    let lat_std_dev = lat_column.std_as_series(1).unwrap();
    let lat_z_scores: Series = (lat_column - lat_mean) / lat_std_dev;

    let lon_column = new_df.column("longitude").unwrap();
    let lon_mean = lon_column.mean().unwrap();
    let lon_std_dev = lon_column.std_as_series(1).unwrap();
    let lon_z_scores: Series = (lon_column - lon_mean) / lon_std_dev;

    let outliers = vec![lat_z_scores.f64().unwrap().cont_slice().unwrap(), lon_z_scores.f64().unwrap().cont_slice().unwrap()];

    //    let average_height = data.column("Height").unwrap().mean().unwrap();
    //  let average_height_string = format!("Average height {}", average_height);

    Consistency { verified: true, outliers: outliers.into() }
*/
    Consistency {
        verified: true,
        outliers: Value::Null,
    }
}
