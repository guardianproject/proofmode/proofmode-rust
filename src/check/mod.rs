pub mod check;
mod consistency;
mod error;
mod integrity;
mod preprocess;
mod synchrony;
mod translate;
pub mod utils;

#[cfg(not(feature = "wasm"))]
use crate::check::check::ProofCheck;
use crate::check::check::check_common;
use crate::check::utils::{fetch_url, ProgressReporter};
#[cfg(not(feature = "wasm"))]
use crate::check::error::ProofModeError;
#[cfg(not(feature = "wasm"))]
use crate::check::utils::ReporterCallback;
#[cfg(not(feature = "wasm"))]
use std::fs::File;
#[cfg(not(feature = "wasm"))]
use std::io::Cursor;
#[cfg(not(feature = "wasm"))]
use std::io::Read;
#[cfg(feature = "wasm")]
use wasm_bindgen::prelude::*;
#[cfg(feature = "wasm")]
use wasm_bindgen::JsValue;
#[cfg(feature = "wasm")]
use crate::check::utils::translate_from_js_files;
#[cfg(feature = "wasm")]
use js_sys::Function;
#[cfg(feature = "wasm")]
use serde_wasm_bindgen::Serializer;
#[cfg(feature = "wasm")]
use serde::Serialize;

#[cfg(feature = "wasm")]
#[wasm_bindgen(js_name = "checkFiles")]
pub async fn check_files(files: JsValue, send_message: &Function) -> Result<JsValue, JsValue> {
    let web_files = translate_from_js_files(files);
    let progress = ProgressReporter::new(send_message.clone());
    let proof_check = check_common(web_files, &progress);
    let serializer = Serializer::new()
        .serialize_maps_as_objects(true)
        .serialize_bytes_as_arrays(false);

    Ok(proof_check.serialize(&serializer).unwrap())
}

#[cfg(not(feature = "wasm"))]
pub fn check_files(
    files: &Vec<String>,
    send_message: ReporterCallback,
) -> Result<ProofCheck, ProofModeError> {
    let input_files = files
        .iter()
        .map(|path| {
            let mut file = File::open(&path).expect(&format!("Could not open file {}", path));
            let mut contents = Vec::new();
            file.read_to_end(&mut contents)
                .expect(&format!("Could not read file {}", path));

            let cursor = Cursor::new(contents);

            preprocess::LocalFile {
                name: path.clone(),
                error: None,
                data: cursor,
            }
        })
        .collect();

    let progress = ProgressReporter::new(send_message.clone());
    let proof_check = check_common(input_files, &progress);

    Ok(proof_check)
}

#[cfg(feature = "wasm")]
#[wasm_bindgen(js_name = "checkURLs")]
pub async fn check_urls(urls: JsValue, send_message: &Function) -> Result<JsValue, JsValue> {
    let urls_array: js_sys::Array = urls.into();
    let mut fetch_files = Vec::new();

    for i in 0..urls_array.length() {
        let url = urls_array.get(i).as_string().unwrap();       
        let res = fetch_url(&url).await.unwrap();
        fetch_files.push(res);
    }

    let progress = ProgressReporter::new(send_message.clone());
    let proof_check = check_common(fetch_files, &progress);
    let serializer = Serializer::new()
        .serialize_maps_as_objects(true)
        .serialize_bytes_as_arrays(false);

    Ok(proof_check.serialize(&serializer).unwrap())
}

#[cfg(not(feature = "wasm"))]
pub fn check_urls(
    urls: &Vec<String>,
    send_message: ReporterCallback,
) -> Result<ProofCheck, ProofModeError> {
    let mut fetch_files = Vec::new();

    for i in 0..urls.len() {
        let url = urls.get(i).unwrap();
        let res = fetch_url(&url)?;
        fetch_files.push(res);
    }

    let progress = ProgressReporter::new(send_message.clone());
    let proof_check = check_common(fetch_files, &progress);

    Ok(proof_check)
}

#[cfg(feature = "wasm")]
#[wasm_bindgen(js_name = "checkCIDs")]
pub async fn check_cids(cids: JsValue, send_message: &Function) -> Result<JsValue, JsValue> {
    let cids_array: js_sys::Array = cids.into();
    let urls_array = js_sys::Array::new();
    for i in 0..cids_array.length() {
        let cid = cids_array.get(i).as_string().unwrap();
        let url = format!("https://cloudflare-ipfs.com/ipfs/{}", cid);
        urls_array.push(&JsValue::from_str(url.as_str()));
    }
    check_urls(urls_array.into(), send_message).await
}

#[cfg(not(feature = "wasm"))]
pub fn check_cids(
    cids: &Vec<String>,
    send_message: ReporterCallback,
) -> Result<ProofCheck, ProofModeError> {
    let mut urls = Vec::new();
    for cid in cids {
        let url = format!("https://cloudflare-ipfs.com/ipfs/{}", cid);
        urls.push(url);
    }
    check_urls(&urls, send_message)
}
