use crate::check::preprocess::ProofCheckFile;
use geo::{coord, Centroid, ConvexHull, LineString, Polygon};
use geojson::{
    Feature, FeatureCollection, GeoJson, Geometry, JsonObject, JsonValue, Value as GeoJSONValue};
use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Synchrony {
    verified: bool,
    geojson: GeoJson,
}

pub fn check_synchrony(proof_files: &Vec<ProofCheckFile>) -> Synchrony {
    let mut features: Vec<Feature> = vec![];
    for file in proof_files {
        if let Some(json) = &file.json {
            if let Some(latitude) = json.location.latitude.clone() {
                if let Some(longitude) = json.location.longitude.clone() {
                    let geometry = Geometry::new(GeoJSONValue::Point(vec![longitude, latitude]));
                    let image_name = file.name.clone();

                    let mut properties = JsonObject::new();
                    properties.insert(String::from("name"), JsonValue::from(image_name));

                    let feature = Feature {
                        bbox: None,
                        geometry: Some(geometry),
                        id: None,
                        properties: Some(properties),
                        foreign_members: None,
                    };

                    features.push(feature);
                }
            }
        }
    }

    let mut coordinates = vec![];

    for feature in features.clone() {
        if let Some(geometry) = feature.geometry {
            if let GeoJSONValue::Point(pt) = geometry.value {
                let coord = coord! {
                   x: pt[0],
                   y: pt[1],
                };

                coordinates.push(coord);
            }
        }
    }

    let coords = LineString(coordinates);
    let polygon = Polygon::new(coords, vec![]);
    let hull = polygon.convex_hull();
    let exterior = hull.exterior();
    let _exterior_string = format!("{:?}", exterior);
    if let Some(centroid) = polygon.centroid() {
        let _centroid_string = format!("{:?}", centroid);
        let geojson_point = GeoJSONValue::Point(vec![centroid.x(), centroid.y()]);

        let mut properties = JsonObject::new();
        properties.insert(String::from("name"), JsonValue::from("Centroid"));

        let feature = Feature {
            bbox: None,
            geometry: Some(Geometry::new(geojson_point)),
            id: None,
            properties: Some(properties),
            foreign_members: None,
        };
        features.push(feature);
    }

    let geojson = FeatureCollection {
        bbox: None,
        features,
        foreign_members: None,
    };

    Synchrony {
        verified: true,
        geojson: geojson.into(),
    }
}
