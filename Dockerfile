FROM rust:latest as builder
WORKDIR /app
COPY . .
RUN cargo build --release

FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y --no-install-recommends libssl-dev
COPY --from=builder /app/target/release/proofmode /usr/local/bin/proofmode
ENTRYPOINT ["/usr/local/bin/proofmode"]
CMD ["--help"]
